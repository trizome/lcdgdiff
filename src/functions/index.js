/* eslint-disable no-unused-vars */
import {
  getDoc,
  getDocs,
  doc,
  query,
  collection,
  where,
} from "firebase/firestore";
import { firestore } from "../firebase-config";
import moment from "moment"




export const findArtistsNames = async (userId) => {
  const ref = doc(firestore, "users", userId);
  const snap = await getDoc(ref);
  return snap.data().artist;
};

export const findTreeLinksChannel = async (currentChannel) => {
  const ref = doc(firestore, "channels", currentChannel);
  const snap = await getDoc(ref);
  const res = snap.data() ? snap.data().treelinks : null;
  return res;
};

export const getChannelsList = async (userUid, listOnly = false) => {
  const q = query(collection(firestore, 'channels'), where('userId', "==", userUid));
  const querySnapshot = await getDocs(q);
  const res = querySnapshot.docs.map(doc => {
    if (listOnly) return { name: doc.id, key: doc.id, value: doc.id, label: doc.data().title }
    return doc.data()
  })
  return res;
};

export const getDefaultEp = async (currentChannel) => {
  const ref = doc(firestore, "channels", currentChannel);

  const snap = await getDoc(ref);
  return snap.data().defaultEp
};


export const getItemsList = async (currentChannel = false, userId = false) => {
  const qAll = query(collection(firestore, 'items'), where('userId', "==", userId));
  const qUUID = query(collection(firestore, 'items'), where('channelId', "==", currentChannel));

  const querySnapshot = currentChannel === 'all' ? await getDocs(qAll) : await getDocs(qUUID);
  const res = querySnapshot.docs.map(doc => {
    return { name: doc.id, key: doc.id, value: doc.id, label: doc.data().title, ...doc.data() }
  })
  return res;
};


export const findChannelByName = async (title) => {
  const q = query(collection(firestore, 'channels'), where('title', "==", decodeURI(title)));
  const querySnapshot = await getDocs(q);
  const channel = querySnapshot.docs[0].data()
  return channel
}


export const findItemByUUID = async (uuid) => {
  const itemRef = doc(firestore, "items", uuid);
  const itemSnap = await getDoc(itemRef);
  const item = itemSnap.data()
  return item
}


export const findChannelByUUID = async (uuid) => {
  const channelRef = doc(firestore, "channels", uuid);
  const channelSnap = await getDoc(channelRef);
  const channel = channelSnap.data()
  return channel
}


export const QRCodeData = async (title) => {
  // recherche du channel
  const channel = await findChannelByName(title)

  // recherche de l'items default
  const item = await findItemByUUID(channel.defaultEp)
  item.audio.url = 'https://us-central1-playerlcdg.cloudfunctions.net/app/stats/' + item.audio.url.split('//')[1]
  return { item, channel }
};

export const playListData = async (title) => {
  // recherche du channel
  const channel = await findChannelByName(title)

  // recherche des items du channel
  const items = await getItemsList(channel.uuid)
  items.forEach(item => {
    item.audio.url = 'https://us-central1-playerlcdg.cloudfunctions.net/app/stats/' + item.audio.url.split('//')[1]
  })
  return { items, channel }
};



export const getListVideoEp = async (userUid, currentChannel) => {
  const testRef = collection(firestore, userUid);
  const q = query(testRef, where(currentChannel, "!=", null));
  const querySnapshot = await getDocs(q);

  const channel = querySnapshot.docs[0].data()[currentChannel]
  channel.rss.channel.item[0].enclosure['@_url'] = 'https://lcdg.s3.fr-par.scw.cloud/Ma Parole EP 2 ft Samuel NADEAU DEF 2 v11-03-2022 Mixdown Master.mp3'
  const listVideoEp = channel.rss.channel.item.map(item => {
    const url = item.enclosure['@_url'].split('//')[1]
    const title = item.title
    const guid = item.guid['#text']
    return { url, title, guid }
  })
  return listVideoEp
};

export const getItemsStats = async (listItems, currentItem) => {
  const testRef = collection(firestore, 'stats');
  currentItem === 'all' && listItems.forEach(i => i.audio.url = decodeURI(i.audio.url).split('//')[1])
  const goodUrlItemsUrls = listItems.map(items => items.audio.url)

  const q = query(testRef, where("param", "in", goodUrlItemsUrls));
  const querySnapshot = await getDocs(q);
  const stats = querySnapshot.docs.sort().reverse().map(doc => {

    const docData = doc.data()

    const formatedDate = moment(docData.date).format('YYYY-MM-DD')
    const findItem = listItems.find(item => {
      return item?.audio?.url === docData.param
    })
    const itemTitle = findItem?.title
    const itemId = findItem?.uuid
    return { ...docData, itemTitle, itemId, formatedDate }
  })
  //return stats
  return currentItem !== 'all' ? stats.filter(i => i.itemId === currentItem) : stats
};


