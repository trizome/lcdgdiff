import { doc, setDoc } from "firebase/firestore";
import { v4 } from "uuid";
import { firestore } from "../firebase-config";
import AWS from 'aws-sdk'
// const config = {
//     accessKeyId: "SCWDSMX7ARGWXCCQC2TC",
//     secretAccessKey: "ad7fa4e0-38cc-4ba8-a2ea-aea7bd81eb51",
//     endpoint: "https://lcdg.s3.fr-par.scw.cloud" /* optional */,
//     ACL: "public-read",
//     forcePathStyle: true,
//     s3ForcePathStyle: true,
//     region: 'fr-par'
// };

const config = {
    accessKeyId: process.env.REACT_APP_SCALEWAY_ACCESS_KEY_ID,
    secretAccessKey: process.env.REACT_APP_SCALEWAY_SECRET_ACCESS_KEY,
    endpoint: process.env.REACT_APP_SCALEWAY_ENDPOINT,
    ACL: "public-read",
    forcePathStyle: true,
    s3ForcePathStyle: true,
    region: process.env.REACT_APP_SCALEWAY_REGION
};

const S3Client = new AWS.S3(config);


const deleteFile = async (name, dest) => {
    try {


        await S3Client.deleteObjects(
            {
                Bucket: 'lcdg',
                Delete: {
                    Objects: [
                        {
                            Key: dest + '/' + name
                        }
                    ]
                }
            }).promise()
    } catch (err) { console.log(err) }
}
const saveFileInScale = async (file, dest, setInUpload, currentUpload) => {

    const name = file.name
    const type = file.type
    const size = file.type
    try {
        const data = await S3Client.upload(
            {
                Bucket: dest,
                Key: name,
                Body: file,
                ACL: "public-read",
                Metadata: {
                    ContentType: type,
                },
            }
        ).on('httpUploadProgress', (evt) => {
            const progress = Math.round((evt.loaded / evt.total) * 100) + "%"
            setInUpload({ currentUpload, progress })
        }).promise()

        return { url: data.Location, type, size, name }
    } catch (err) { console.log(err) }
}


export const saveItem = async (values, currentChannel, currentUser, setInUpload) => {
    values.pubDate = values.pubDate.format('LLLL')
    values.uuid = v4() // if ADD SEULEMENT
    values.channelId = currentChannel
    values.userId = currentUser
    const { image, audio, ...itemValues } = values
    //
    //  ADD IMAGE TO SCALEWAY
    const destImage = `${currentUser}/items/${values.uuid}/image`;
    const locationImage = await saveFileInScale(image[0].originFileObj, destImage, setInUpload, 'image')
    itemValues.image = {
        link: locationImage.url,
        title: locationImage.name,
        url: locationImage.url
    }
    //
    //  ADD AUDION TO SCALEWAY // 
    const destAudio = `${currentUser}/items/${values.uuid}/audios`;

    const locationAudio = await saveFileInScale(audio[0].originFileObj, destAudio, setInUpload, 'audio')
    itemValues.audio = {
        link: locationAudio.url,
        title: locationAudio.name,
        url: locationAudio.url
    }
    itemValues.audio = {
        link: locationAudio.url,
        title: locationAudio.name,
        url: locationAudio.url
    };

    await setDoc(doc(firestore, 'items', values.uuid), itemValues)
    const itemId = values.uuid;
    const audioParams = {
        '@_length': locationAudio.size,
        '@_type': locationAudio.type,
        '@_url': locationAudio.url,
        itemId,
        channelId: currentChannel,
        userId: currentUser,
    }
    await setDoc(doc(firestore, 'audios', values.uuid), audioParams)
    setInUpload(false)
    return itemId
}

export const saveChannel = async (values, currentUser, setInUpload, channelId, oldFiles) => {
    values.uuid = channelId || v4() // if ADD SEULEMENT
    values.userId = currentUser
    const { image, ...channelValues } = values
    //
    // si une nouvelle image a ete envoyé
    if (image[0]?.originFileObj) {
        //  define path of image
        const destImage = `${currentUser}/channels/${values.uuid}`;
        // si une image existait deja on la supprime
        const oldFileName = oldFiles ? oldFiles[0].name : null
        oldFileName && deleteFile(oldFileName, destImage)

        // add new image 
        const locationImage = await saveFileInScale(image[0].originFileObj, destImage, setInUpload, 'image')
        channelValues.image = {
            link: locationImage.url,
            title: locationImage.name,
            url: locationImage.url
        }
    } else {
        channelValues.image = {
            link: oldFiles[0].url,
            title: oldFiles[0].name,
            url: oldFiles[0].url
        }
    }


    await setDoc(doc(firestore, 'channels', values.uuid), channelValues)
    channelId = channelId || values.uuid;

    setInUpload(false)
    return channelId
}


