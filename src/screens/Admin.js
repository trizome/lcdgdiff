import React, { useState } from 'react';
import { Menu, Row, Col, Grid } from 'antd';
import Channels from '../components/Channels';
import Statistics from '../components/Statistics';
import Artist from '../components/Artist';
import { Navigate } from 'react-router-dom';
import { BarChartOutlined, AudioOutlined, EuroCircleOutlined, UserOutlined } from '@ant-design/icons';
import { useAuthValue } from "./../providers/AuthContext";

const { useBreakpoint } = Grid;

const baseMenu = [
    {
        key: `channels`,
        icon: React.createElement(AudioOutlined),
        label: `Mes podcasts`,
        component: <Channels />,
    },
    {
        key: `artist`,
        icon: React.createElement(UserOutlined),
        label: `Artiste`,
        component: <Artist />
    },
    {
        key: `statistics`,
        icon: React.createElement(BarChartOutlined),
        label: `Statistiques`,
        component: <Statistics />
    },
    {
        key: `cost`,
        icon: React.createElement(EuroCircleOutlined),
        label: `Coûts`,
        component: <Statistics />
    },
]

const Admin = () => {
    const { currentUser } = useAuthValue()
    const [currentComponent, setCurrentComponent] = useState(baseMenu[0].component)
    const screensSize = useBreakpoint();

    if (!currentUser?.emailVerified) {
        return <Navigate to='/' replace />
    }

    const onClick = (e) => {
        const newcomponent = baseMenu.find(item => item.key === e.key).component
        setCurrentComponent(newcomponent)
    };
    const isXs = !screensSize.lg

    return (
        <>
            <Row >
                <Col span={isXs ? 24 : 4}>
                    <Menu
                        onClick={onClick}
                        mode={isXs ? "horizontal" : "inline"}
                        defaultSelectedKeys={['channels']}
                        style={{ height: '100%' }}
                        items={baseMenu}
                    />
                </Col>

                <Col span={isXs ? 24 : 20}>
                    <div className='content' >
                        {currentComponent && React.cloneElement(currentComponent)}
                    </div>
                </Col>
            </Row>
        </>
    )
}
export default Admin