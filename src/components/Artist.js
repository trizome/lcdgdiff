import { Button, Col, Row, Typography } from "antd";
import React from "react";
const { Title } = Typography;

const Artist = () => {
    return (<>
        <Row gutter={24} style={{ width: '70%', minWidth: 300, maxWidth: 700, margin: '24px auto' }}>
            <Col xs={24} sm={8} style={{ textAlign: 'center' }}><img style={{ width: '100%', maxWidth: 300 }} src='https://res.cloudinary.com/pippa/image/fetch/h_500,w_500,f_auto/https://assets.pippa.io/shows/5e04ebab880ab054252decf0/1613333270325-0fdb772c1ad8f62ff103cb203bc75570.jpeg' alt='LCDG podcast' /></Col>
            <Col xs={24} sm={16}>
                <Title level={3}>Kevy Shako</Title>
                <div>
                    Le podcast Urbain
                    Comment un gus timide au possible,
                    limite proche de l’autisme peut-il, de fil en aiguille, devenir l aimant qui attire toute la misère du monde ?
                    Je suis l’élu. Ou juste un quadra à la voix gravissime qui se rend compte
                </div>
                <div style={{ marginTop: 12 }}>Email : kshako@gmail.com</div>
                <div style={{ marginTop: 12 }}>Tel : 06 23 23 24 25</div>
                <div style={{ marginTop: 12 }}><Button type='primary'>Editer</Button></div>
            </Col>
        </Row>

    </>
    )
}
export default Artist