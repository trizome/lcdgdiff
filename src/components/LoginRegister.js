import React, { useState, useEffect } from 'react'

import { auth } from '../firebase-config'
import { useNavigate, Link } from 'react-router-dom'
import { createUserWithEmailAndPassword, sendEmailVerification } from 'firebase/auth'
import { useAuthValue } from '../providers/AuthContext'
import {
    Button,
    Form,
    Input,
    Modal
} from 'antd';
import { doc, setDoc } from "firebase/firestore";
import { firestore } from "../firebase-config";


const formItemLayout = {
    labelCol: {
        xs: {
            span: 24,
        },
        sm: {
            span: 8,
        },
    },
    wrapperCol: {
        xs: {
            span: 24,
        },
        sm: {
            span: 16,
        },
    },
};

const tailFormItemLayout = {
    wrapperCol: {
        xs: {
            span: 24,
            offset: 0,
        },
        sm: {
            span: 16,
            offset: 8,
        },
    },
};

function LoginRegister() {
    const [form] = Form.useForm();


    const [error, setError] = useState('')
    const navigate = useNavigate()
    const [visibleModal, setVisibleModal] = useState(false)
    //const { setTimeActive } = useAuthValue()
    const [standValidation, setStandValidation] = useState(false)
    const { currentUser } = useAuthValue()

    useEffect(() => {
        const interval = setInterval(() => {
            currentUser?.reload()
                .then(async () => {
                    setStandValidation(standValidation + 1)
                    if (currentUser?.emailVerified) {
                        const userRef = doc(firestore, "users", currentUser.uid);
                        try {
                            await setDoc(userRef, { email: currentUser.email, artist: { name: null, image: null, email: currentUser.email, phone: null, acces: null } }, { merge: true });
                            clearInterval(interval)
                            setVisibleModal(false)
                            navigate('/admin')
                        } catch (e) {
                            alert(e);
                        }

                    }
                })
                .catch((err) => {
                    alert(err.message)
                })
        }, 1000)

    }, [currentUser])

    const register = async (values) => {
        const { email, password, /*artist */ } = values
        setError('')

        try {
            await createUserWithEmailAndPassword(auth, email, password).catch((err) =>
                console.log(err)
            );

            await sendEmailVerification(auth.currentUser).catch((err) =>
                console.log(err)
            );

            // save artist
            // const valuesArtist = { artist, description: null, email, phone: null, userId: auth.currentUser.uid }
            // console.log(auth.currentUser, { valuesArtist })
            // await setDoc(doc(firestore, 'artists', currentUser.uid), valuesArtist)

            setStandValidation(true)


        } catch (err) {
            console.log(err);
        }



    }

    return (
        <><Button onClick={() => setVisibleModal(true)}>Register</Button>
            <Modal title="Basic Modal" visible={visibleModal} onCancel={() => setVisibleModal(false)}>
                {standValidation ? <div>En attente de validation du mail</div> :
                    <div className='center'>
                        <div className='auth'>
                            <h1>Register</h1>
                            {error && <div className='auth__error'>{error}</div>}



                            <Form
                                {...formItemLayout}
                                form={form}
                                name="register"
                                onFinish={register}

                                scrollToFirstError
                            >
                                {/* <Form.Item
                                    style={{ marginBottom: 12 }}
                                    name="artist"
                                    label="Nom de podcaster"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'SVP remplissez votre nom de podcaster!',
                                        },
                                    ]}
                                    help="Obligatoire : C'est le nom qui apparaitra sur les plateforme comme auteur"
                                >
                                    <Input />
                                </Form.Item> */}
                                <Form.Item
                                    name="email"
                                    label="E-mail"
                                    rules={[
                                        {
                                            type: 'email',
                                            message: "Cet email n'est pas valide!",
                                        },
                                        {
                                            required: true,
                                            message: 'SVP remplissez votre E-mail!',
                                        },
                                    ]}
                                >
                                    <Input />
                                </Form.Item>

                                <Form.Item
                                    name="password"
                                    label="Password"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Please input your password!',
                                        },
                                    ]}
                                    hasFeedback
                                >
                                    <Input.Password />
                                </Form.Item>

                                <Form.Item
                                    name="confirm"
                                    label="Confirm Password"
                                    dependencies={['password']}
                                    hasFeedback
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Please confirm your password!',
                                        },
                                        ({ getFieldValue }) => ({
                                            validator(_, value) {
                                                if (!value || getFieldValue('password') === value) {
                                                    return Promise.resolve();
                                                }

                                                return Promise.reject(new Error('The two passwords that you entered do not match!'));
                                            },
                                        }),
                                    ]}
                                >
                                    <Input.Password />
                                </Form.Item>
                                <Form.Item {...tailFormItemLayout}>
                                    <Button type="primary" htmlType="submit">
                                        Register
                                    </Button>
                                </Form.Item>
                            </Form>
                            <span>
                                {"J'ai déjà un compte ? "}<Link to='/login'>login</Link>
                            </span>
                        </div>
                    </div>}
            </Modal>
        </>
    )
}

export default LoginRegister