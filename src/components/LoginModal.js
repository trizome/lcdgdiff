import React, { useState } from "react"
import { Link } from 'react-router-dom'
import { signInWithEmailAndPassword, sendEmailVerification } from 'firebase/auth'
import { auth } from './../firebase-config'
import { useNavigate } from 'react-router-dom'
import { useAuthValue } from './../providers/AuthContext'
import { Button, Modal } from "antd"

const LoginModal = () => {
    const [visibleModal, setVisibleModal] = useState(false)
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [error, setError] = useState('')
    const { setTimeActive } = useAuthValue()
    const navigate = useNavigate()

    const login = e => {
        e.preventDefault()
        signInWithEmailAndPassword(auth, email, password)
            .then(() => {
                if (!auth.currentUser.emailVerified) {
                    sendEmailVerification(auth.currentUser.displayName)
                        .then(() => {
                            setTimeActive(true)
                            console.log('pas trouvé')
                        })
                        .catch(err => alert(err.message))
                } else {
                    navigate('admin')
                }
            })
            .catch(err => setError(err.message))
    }

    return (
        <>

            <Button onClick={() => setVisibleModal(true)}>Connexion</Button>

            <Modal title="Basic Modal" visible={visibleModal} onCancel={() => setVisibleModal(false)}>
                <div className='center'>
                    <div className='auth'>
                        <h1>Log in</h1>
                        {error && <div className='auth__error'>{error}</div>}
                        <form onSubmit={login} name='login_form'>
                            <input
                                type='email'
                                value={email}
                                required
                                placeholder="Enter your email"
                                onChange={e => setEmail(e.target.value)} />

                            <input
                                type='password'
                                value={password}
                                required
                                placeholder='Enter your password'
                                onChange={e => setPassword(e.target.value)} />

                            <button type='submit'>Login</button>
                        </form>
                        <p>
                            Je n&apos;ai pas de compte?
                            <Link to='/register'>Create one here</Link>
                        </p>
                    </div>
                </div>
            </Modal>
        </>
    )
}

export default LoginModal