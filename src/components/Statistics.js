/* eslint-disable no-unused-vars */
import React, { useState, useEffect, useRef } from "react";
import { useAuthValue } from "./../providers/AuthContext";
import { getChannelsList, getItemsList, getItemsStats } from "../functions"
import { Select, Col, Row, Table, DatePicker, Typography } from "antd";
//import { ExclamationCircleOutlined, DeleteOutlined, QrcodeOutlined, SearchOutlined } from "@ant-design/icons";

import { LineChart, Line, XAxis, YAxis, CartesianGrid, BarChart, Bar, Cell, Tooltip, Legend } from 'recharts';

const { RangePicker } = DatePicker;
const { Title, Text } = Typography;

const { Option } = Select;
const COLORS = ['#0088FE', '#00C49F', '#FFBB28', '#FF8042'];
const RADIAN = Math.PI / 180;
const renderCustomizedLabel = ({ cx, cy, midAngle, innerRadius, outerRadius, percent, index }) => {
    const radius = innerRadius + (outerRadius - innerRadius) * 0.5;
    const x = cx + radius * Math.cos(-midAngle * RADIAN);
    const y = cy + radius * Math.sin(-midAngle * RADIAN);

    return (
        <text x={x} y={y} fill="white" textAnchor={x > cx ? 'start' : 'end'} dominantBaseline="central">
            {`${(percent * 100).toFixed(0)}%`}
        </text>
    );
};
const visitesColumns = [
    {
        title: "Episode",
        dataIndex: "itemTitle",
        key: "itemTitle",
    },
    {
        title: "Pays",
        dataIndex: "country",
        key: "country",
    },
    {
        title: "Date",
        dataIndex: "formatedDate",
        key: "formatedDate",
    },]
const statsbydaysColumns = [
    {
        title: "date",
        dataIndex: "date",
        key: "date",
    },
    {
        title: "Nombre d'écoutes",
        dataIndex: "total",
        key: "total",
    }]

const getDataByDay = (resData) => {
    var data = {};
    for (var entry of resData) {
        const { formatedDate } = entry
        data[formatedDate] = data[formatedDate] || 0;
        data[formatedDate]++
    }
    const array = Object.keys(data).sort().reduce((accumulator, currentValue) => {
        accumulator[currentValue] = data[currentValue];
        return accumulator;
    }, {})
    return Object.keys(array).map(key => { return { day: key, visite: data[key] } })
    //return Object.keys(data).map(key => { return { day: key, visite: data[key] } })
}
const getDataByReferer = (resData) => {
    var data = {};
    for (var entry of resData) {
        const { referer } = entry
        data[referer] = data[referer] || 0;
        data[referer]++
    }
    const array = Object.keys(data).sort().reduce((accumulator, currentValue) => {
        accumulator[currentValue] = data[currentValue];
        return accumulator;
    }, {})
    return Object.keys(array).map(key => { return { referer: key, visite: data[key] } }).filter(entry => entry.referer !== 'null')
    //return Object.keys(data).map(key => { return { day: key, visite: data[key] } })
}
const Statistics = () => {
    const { currentUser } = useAuthValue()
    const [stats, setStats] = useState();
    const [statsByDay, setStatsByDay] = useState();
    const [items, setItems] = useState();
    const [itemListVideos, setItemListVideos] = useState();
    const [currentChannel, setCurrentChannel] = useState();
    const [currentItem, setCurrentItem] = useState();
    const [loading, setLoading] = useState(true);
    const [channelsList, setChannelsList] = useState();
    const [itemsList, setItemsList] = useState();
    const [refresh, setRefresh] = useState(false);
    const [refreshStats, setRefreshStats] = useState(false);
    const [dataDay, setDataDay] = useState([])
    const [dataReferer, setDataReferer] = useState([])
    const table1ref = useRef(null);
    const [table1Size, table1SizeSet] = useState({
        width: undefined,
        height: undefined,
    });

    useEffect(() => {

        const fetchListChannels = async () => {
            const res = await getChannelsList(currentUser.uid, true);
            const firstChannel = { value: 'all' };

            setCurrentChannel(firstChannel);
            setChannelsList(res);
            setRefresh(true)
        };
        fetchListChannels();
    }, [])

    useEffect(() => {
        const fetchListItems = async () => {
            const res = await getItemsList(currentChannel.value, currentUser.uid);

            res.forEach(item => item.key = item.uuid)
            const firstItem = { value: 'all' };
            setCurrentItem(firstItem);
            setItemsList(res);
            setLoading(false);
            setRefresh(false);
            setRefreshStats(true)
        };
        refresh && fetchListItems();
    }, [refresh]);

    useEffect(() => {

        const fetchStats = async () => {
            const resStats = itemsList.length ? await getItemsStats(itemsList, currentItem.value) : [];
            setStats(resStats)
            setDataDay(getDataByDay(resStats))
            setDataReferer(getDataByReferer(resStats))

            setRefreshStats(false)
        };
        refreshStats && fetchStats();
    }, [refreshStats]);
    useEffect(() => {
        function handleResize() {
            table1SizeSet({
                width: table1ref?.current?.offsetWidth ? table1ref.current.offsetWidth : 0,
                height: table1ref?.current?.offsetHeight ? table1ref.current.offsetHeight : 0,
            });
        }
        stats && table1ref?.current !== null && window.addEventListener("resize", handleResize);
        stats && table1ref?.current !== null && handleResize();
        return () => window.removeEventListener("resize", handleResize);

    }, [stats]);




    const handleChangeCurrentChannel = (e) => {
        setLoading(true)
        setCurrentChannel(e)
        setRefresh(true)
    }
    const handleChangeCurrentItem = (e) => {
        setLoading(true)
        setCurrentItem(e)
        setRefreshStats(true)
    }
    const sizeGraph = table1Size.width ? table1Size.width - 60 : 240
    return <>
        <>
            <Title type="success" level={5} style={{ padding: '12px 24px 0px 24px' }} >STATISTICS</Title>

            <Row style={{ padding: '12px 24px 4px 24px', fontWeight: 700, width: "100%", borderBottom: "1px solid #ddd" }} align="middle">
                {channelsList && (
                    <Col md={24} lg={8} style={{ marginBottom: 12 }}>
                        Mes podcast :{" "}
                        <Select
                            value={currentChannel}
                            style={{ width: 300 }}
                            onChange={(e) => handleChangeCurrentChannel(e)}
                            labelInValue
                        >
                            <Option value={'all'} key={'all'}>
                                Tous mes podcast
                            </Option>
                            {channelsList.map((c) => (
                                <Option value={c.name} key={c.name}>
                                    {c.label}
                                </Option>
                            ))}

                        </Select>
                    </Col>
                )}
                {itemsList && currentChannel.value !== 'all' && (

                    <Col md={24} lg={8} style={{ marginBottom: 12 }}>
                        Mes épisodes :{" "}
                        <Select
                            value={currentItem}
                            style={{ width: 300 }}
                            onChange={(e) => handleChangeCurrentItem(e)}
                            labelInValue
                        >
                            <Option value={'all'} key={'all'}>
                                Tous les épisodes
                            </Option>
                            {itemsList.map((c) => (
                                <Option value={c.name} key={c.name}>
                                    {c.label}
                                </Option>
                            ))}

                        </Select>
                    </Col>


                )}
                <Col md={24} lg={8} style={{ marginBottom: 12 }}>
                    Période :{" "}<RangePicker />
                </Col>
            </Row>

        </>

        {stats && stats.length && (<>
            <Row >
                <Col xs={24} sm={24} md={12} ref={table1ref} >
                    <div style={{ boxShadow: '1px 1px 4px 0 #00000022', paddin: 12, margin: 12 }}>
                        <div ><Text strong type="success"  >Visites par jours</Text></div>
                        <LineChart width={sizeGraph} height={200} data={dataDay}>
                            <XAxis dataKey="day" />
                            <YAxis />
                            <CartesianGrid stroke="#eee" strokeDasharray="5 5" />
                            <Line type="monotone" dataKey="visite" stroke="#8884d8" />
                        </LineChart>
                    </div>
                </Col>
                <Col xs={24} sm={24} md={12}  >
                    <div style={{ boxShadow: '1px 1px 4px 0 #00000022', paddin: 12, margin: 12 }}>
                        <div ><Text strong type="success"  >Visites par jours</Text></div>
                        <BarChart
                            width={sizeGraph} height={200}
                            data={dataReferer}
                        >
                            <CartesianGrid strokeDasharray="3 3" />
                            <XAxis dataKey="referer" />
                            <YAxis />
                            <Tooltip />
                            {/* <Legend /> */}
                            <Bar dataKey="visite" fill="#8884d8" />
                            {/* <Bar dataKey="uv" fill="#82ca9d" /> */}
                        </BarChart>
                    </div>
                </Col>

            </Row>
            <Table
                style={{ width: 'calc(100% -24px)', marginLeft: 12, marginRight: 12 }}
                columns={visitesColumns}
                dataSource={stats}
            />
        </>
        )}
    </>
}

export default Statistics

