/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */
import React, { useState } from "react";
import { Button, Checkbox, Form, Input, Upload, Select, InputNumber, DatePicker, Modal, Row, Col } from 'antd';
import { PictureOutlined, PlusOutlined } from "@ant-design/icons";
import { saveChannel } from "../functions/saveData";
import moment from 'moment'
import {
    doc, setDoc
} from "firebase/firestore";
import { firestore } from "../firebase-config";
import { AuthProvider } from "./providers/AuthContext";
import { auth } from './firebase-config'


const EditArtist = (props) => {
    const { currentUser } = props
    const { currentUser } = useAuthValue()

    const [isModalVisible, setIsModalVisible] = useState(false);
    const [form] = Form.useForm();

    const onFinish = async (values) => {
        console.log(values)
        setCurrentUser({ ...user, artist })
        const userRef = doc(firestore, "users", currentUser.uid);
        await setDoc(userRef, { artist: { name: null } }, { merge: true });
    };

    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };




    return (<>
        <Button icon={<PlusOutlined />} block={props.block} style={props.style} type='primary' danger onClick={() => setIsModalVisible(true)}>ADD PODCAST</Button>
        <Modal
            footer={null}
            title={props.title || 'rien'} visible={isModalVisible} onOk={() => setIsModalVisible(false)} onCancel={() => setIsModalVisible(false)}>

            {inUpload?.currentUpload ? <div><span>{inUpload.currentUpload}</span>{' : '}<span>{inUpload.progress}</span></div> :
                < Form
                    layout={"vertical"}
                    onFinish={onFinish}
                    onFinishFailed={onFinishFailed}
                    autoComplete="off"
                    initialValues={currentValues || {}}
                    form={form}
                >



                    {/*  TITLE */}
                    <Form.Item
                        label="Nom de l'artist (auteur)"
                        name="name"
                        rules={[
                            {
                                required: true,
                                message: "Le nom d'artists est obligatoire !",
                            },
                        ]}
                        help="Le nom d'artists est obligatoire ! c'est l'auteur des podcast"
                    >
                        <Input />
                    </Form.Item>



                    <Form.Item
                        wrapperCol={{
                            offset: 0,
                            span: 24,
                        }}
                        style={{ textAlign: "center" }}
                    >
                        <Button  >
                            Annuler
                        </Button>
                        <Button type="primary" htmlType="submit">
                            Valider
                        </Button>
                    </Form.Item>
                </Form >}
        </Modal ></>
    );
};

export default EditArtist