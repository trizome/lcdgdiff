/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */

import React, { useState, useEffect } from "react";
import { Button, Form, Input, Select, InputNumber, DatePicker, Modal, Row, Col } from 'antd';
import {
    PlusOutlined, EditOutlined
} from "@ant-design/icons";
import { saveItem } from "../functions/saveData";
import UploadCustom from "./UploadCustom";
import { findItemByUUID } from "../functions/";

import moment from 'moment';

const { Option } = Select;

const EditItem = (props) => {
    const { uid, currentItem = null, currentChannel, currentUser } = props
    const itemId = currentItem ? currentItem.uuid : null;
    const [data, setdata] = useState();
    const [isModalVisible, setIsModalVisible] = useState(false);
    const [inUpload, setInUpload] = useState(false);
    const [form] = Form.useForm();
    const [loading, setLoading] = useState(!!uid);

    useEffect(() => {
        const fetchItem = async () => {
            const resItem = await findItemByUUID(itemId)
            resItem.pubDate = moment(resItem.pubDate);
            const { title: titleImage, url: urlImage = null } = resItem.image
            resItem.defaultFileListImage = [{
                uuid: resItem.uuid, name: titleImage, url: urlImage, source: "bdd",
            }]
            const { title: titleAudio, url: urlAudio = null } = resItem.audio
            resItem.defaultFileListAudio = [{
                uuid: resItem.uuid, name: titleAudio, url: urlAudio, source: "bdd",
            }]
            setdata(resItem)
            setLoading(false)
        }
        itemId ? fetchItem() : setLoading(false)

    }, [itemId])

    const onFinish = async (values) => {
        const itemId = await saveItem(values, currentChannel, currentUser.uid, setInUpload)
        setIsModalVisible(false)
        props.setRefresh(true)
    };

    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };


    return (
        <>
            {props.icon
                ? <EditOutlined style={props.style} onClick={() => setIsModalVisible(true)} />
                : <Button icon={<PlusOutlined />} style={props.style} type="primary" onClick={() => setIsModalVisible(true)}>
                    Add épisode
                </Button>}
            <Modal
                footer={null}
                title={props.item ? 'Modifier un épisode' : 'Ajouter un épisode'} visible={isModalVisible} onOk={() => setIsModalVisible(false)} onCancel={() => setIsModalVisible(false)}>

                {inUpload?.currentUpload ? <div><span>{inUpload.currentUpload}</span>{' : '}<span>{inUpload.progress}</span></div> :
                    < Form
                        layout={"vertical"}
                        onFinish={onFinish}
                        onFinishFailed={onFinishFailed}
                        autoComplete="off"
                        form={form}
                        initialValues={data || {}}
                    >


                        {/*  TITLE */}
                        <Form.Item
                            label="Titre de l'épisode"
                            name="title"
                            rules={[
                                {
                                    required: true,
                                    message: 'Please input a title!',
                                },
                            ]}
                        >
                            <Input />
                        </Form.Item>

                        {/*  IMAGE TEST*/}
                        <UploadCustom setFieldsValue={form.setFieldsValue} fieldName='image' defaultFileList={data?.defaultFileListImage} />


                        {/*  MP3 */}
                        <UploadCustom setFieldsValue={form.setFieldsValue} fieldName='audio' defaultFileList={data?.defaultFileListAudio} />


                        {/*  DESCRIPTION */}
                        <Form.Item
                            label="Description"
                            name="description"
                            rules={[
                                {
                                    required: true,
                                    message: 'enter a description!',
                                },
                            ]}
                            extra="Saisissez une description de l'épisode"
                        >
                            <Input.TextArea showCount rows={4} />
                        </Form.Item>

                        <Row>
                            <Col span={12}>
                                {/*  EPISODETYPE */}
                                <Form.Item
                                    label="Type d'épisode"
                                    name="episodeType"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'enter a type!',
                                        },
                                    ]}
                                    style={{ width: '80%' }}
                                >
                                    <Select >
                                        <Option value="full">Full</Option>
                                        <Option value="trailer">Trailer</Option>
                                        <Option value="bonus">Bonus</Option>
                                    </Select>

                                </Form.Item>
                            </Col>
                            <Col span={12}>
                                {/*  EXPLICIT */}
                                <Form.Item
                                    label="Language Explicite"
                                    name="explicit"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'enter a explicit!',
                                        },

                                    ]}
                                    style={{ width: '80%' }}
                                >
                                    <Select >
                                        <Option value="true">Oui</Option>
                                        <Option value="false">NON</Option>
                                    </Select>

                                </Form.Item>
                            </Col>

                        </Row>

                        <Row>
                            <Col span={12}>
                                {/*  SAISON */}
                                <Form.Item
                                    label="Saison"
                                    name="saison"
                                >
                                    <InputNumber min={1} max={100} />
                                </Form.Item>
                            </Col>
                            <Col span={12}>
                                {/*  PUBDATE */}
                                <Form.Item
                                    label="Date de publication"
                                    name="pubDate"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'enter a date!',
                                        },
                                    ]}
                                >
                                    <DatePicker />
                                </Form.Item>
                            </Col>

                        </Row>
                        {/*  QRCODE */}
                        {/* <Form.Item
                        name="defaultEp"
                        valuePropName="checked"
                        wrapperCol={{
                            offset: 2,
                            span: 22,
                        }}
                    >
                        <Checkbox>Cet épisode devient la cible du qr code</Checkbox>
                    </Form.Item> */}

                        <Form.Item
                            wrapperCol={{
                                offset: 0,
                                span: 24,
                            }}
                            style={{ textAlign: "center" }}
                        >
                            <Button  >
                                Annuler
                            </Button>
                            <Button type="primary" htmlType="submit">
                                Valider
                            </Button>
                        </Form.Item>
                    </Form >}
            </Modal ></>
    );
};

export default EditItem