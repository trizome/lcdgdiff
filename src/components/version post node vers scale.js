import React, { useState, useEffect } from "react";
import moment from 'moment'
import { useAuthValue } from "./../providers/AuthContext";
import { Link } from "react-router-dom";

import { getChannelsList, getItemsList } from "../functions"
import { Select, Button, Col, Row, Table, Form, Input, Upload } from "antd";
import { PlusOutlined, CheckOutlined } from "@ant-design/icons";
import PopLinks from '../components/PopLinks';

const { Option } = Select;
const columns = [
    {
        title: "Titre",
        dataIndex: "title",
        key: "title",
    },
    {
        title: "Date",
        dataIndex: "date",
        key: "date",
        render: (_, record) => moment(record.date).format("DD-MM-YYYY"),
    },
    {
        title: "QR code",
        dataIndex: "qrcode",
        key: "qrcode",
        render: (_, record) => (record.qrcode ? <CheckOutlined /> : ""),
    },
    {
        title: "Action",
        key: "action",
        render: (_, record) => {
            return record.qrcode && record.chaine ? (
                <Link
                    to={"/" + record.chaine}
                    target="_blank"
                    rel="noopener noreferrer"
                >
                    view
                </Link>
            ) : (
                ""
            );
        },
    },
];

const Channels = () => {
    const { currentUser } = useAuthValue()
    const [data, setData] = useState();
    const [currentChannel, setCurrentChannel] = useState();
    const [loading, setLoading] = useState(true);
    const [addOpen, setAddOpen] = useState(false);
    const [channelsList, setChannelsList] = useState();
    const [refresh, setRefresh] = useState(false);
    //const [selectedRowKeysPerso, setSelectedRowKeysPerso] = useState();
    console.log({
        data,
        loading,
        addOpen
    })
    useEffect(() => {
        console.log('useeffect')
        const fetchListChannels = async () => {
            const res = await getChannelsList(currentUser.uid);
            const firstChannel = res ? res[0].name : null;
            setChannelsList(res);
            setCurrentChannel(firstChannel);
        };
        fetchListChannels();
    }, [])

    useEffect(() => {
        const fetchListItems = async () => {
            const res = await getItemsList(currentUser.uid, currentChannel);
            setData(res);
            setLoading(false);
            setRefresh(false);
            setAddOpen(false);
        };
        currentChannel && fetchListItems();

    }, [currentChannel, refresh]);

    const chaineLabel = currentChannel
        ? channelsList.find((c) => c.name === currentChannel)
        : null;


    const uploadFile = file => {
        console.log(file)
        const API_ENDPOINT = "https://file.io";
        const request = new XMLHttpRequest();
        const formData = new FormData();

        request.open("POST", API_ENDPOINT, true);
        request.onreadystatechange = () => {
            if (request.readyState === 4 && request.status === 200) {
                console.log(request.responseText);
            }
        };
        formData.append("file", file);
        console.log(formData)
        request.send(formData);
    };

    const normFile = (e) => {
        console.log('Upload event:', e);

        if (Array.isArray(e)) {
            return e;
        }

        return e?.fileList;
    };
    return <>
        {channelsList && (
            <Row style={{ marginLeft: 12, fontWeight: 700 }} gutter={8} align="middle">
                <Col style={{ marginBottom: 12 }}>
                    Chaines :{" "}
                    <Select
                        value={currentChannel}
                        style={{ width: 300 }}
                        //onChange={handleChangeChaine}
                        size="large"
                    >
                        {channelsList.map((c) => (
                            <Option value={c.name} key={c.name}>
                                {c.label}
                            </Option>
                        ))}
                    </Select>
                </Col>
                {currentChannel && (
                    <>
                        <Col style={{ marginBottom: 12 }}>
                            {<PopLinks chaineLabel={chaineLabel} />}
                        </Col>
                        <Col style={{ marginBottom: 12 }}>
                            {<PopLinks chaineLabel={chaineLabel} />}
                        </Col>
                        <Col style={{ marginBottom: 12, textAlign: "right" }}>
                            <Button
                                onClick={() => setAddOpen(true)}
                                icon={<PlusOutlined />}
                            >
                                Ajouter un épisode
                            </Button>
                        </Col>
                        {/* <Col style={{ marginBottom: 12 }}>
                            {<PopQR chaineLabel={chaineLabel} />}
                        </Col> */}
                    </>
                )}
            </Row>
        )}
        {!loading && (
            <> <Table
                //   rowSelection={{
                //     selectedRowKeys: selectedRowKeysPerso,
                //     type: "radio",
                //     ...rowSelection,
                //   }}
                columns={columns}
                dataSource={data}
            />
                <Form
                    name="validate_other"
                    onFinish={uploadFile}
                    initialValues={{

                    }}
                >
                    <Form.Item
                        label="Username"
                        name="username"
                        rules={[
                            {
                                required: true,
                                message: 'Please input your username!',
                            },
                        ]}
                    >
                        <Input />
                    </Form.Item>

                    <Form.Item
                        name="upload"
                        label="Upload"
                        valuePropName="fileList"
                        getValueFromEvent={normFile}
                        extra="longgggggggggggggggggggggggggggggggggg"
                    >
                        <Upload beforeUpload={() => false} name="logo" listType="picture">
                            <Button >Click to upload</Button>
                        </Upload>
                    </Form.Item>



                    <Form.Item
                        wrapperCol={{
                            offset: 8,
                            span: 16,
                        }}
                    >
                        <Button type="primary" htmlType="submit">
                            Submit
                        </Button>
                    </Form.Item>
                </Form>
            </>
        )}
    </>
}
export default Channels

