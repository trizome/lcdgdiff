/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */

import React, { useState } from "react";
import { Button, Form, Upload } from 'antd';
import {
    SwapOutlined,
    PictureFilled,
    DeleteOutlined
} from "@ant-design/icons";


const UploadCustom = (props) => {
    const { setFieldsValue, fieldName, defaultFileList = [] } = props
    const [fileListField, setFileListField] = useState(defaultFileList);
    function onChange(e, field) {
        const val = e.length
            ? e.map((v) => {
                return v.from ? v : { ...v, from: "front" };
            })
            : [];
        setFileListField(val);
        setFieldsValue({ [fieldName]: val });
    }
    const deleteFile = (field) => {
        setFileListField([]);
        setFieldsValue({ [fieldName]: [] });
    };


    let fileToUrl;
    if (fileListField[0]?.originFileObj) {

        console.log(fileListField[0]?.originFileObj)
        fileToUrl = URL.createObjectURL(fileListField[0].originFileObj);
    } else if (fileListField[0]?.url) {
        fileToUrl = fileListField[0].url;
    } else {
        fileToUrl = null;
    }
    console.log(fileToUrl)
    return (
        <Form.Item
            name={fieldName}
            rules={[
                {
                    required: true,
                    message: "Please input your image!",
                },
            ]}
        >
            {!fileListField.length ? (
                <Upload.Dragger
                    maxCount={1}
                    beforeUpload={() => false}
                    onChange={(e) => onChange(e.fileList, { fieldName })}
                    fileList={fileListField}
                    showUploadList={false}
                >
                    <div style={{ color: "#666", texAlign: "center" }}>
                        <PictureFilled
                            style={{ color: "#40a9ff", fontSize: 24, marginBottom: 12 }}
                        />
                        <div style={{ fontWeight: 400, fontSize: ".9em" }}>
                            Cliquez ici pour
                        </div>
                        <div style={{ fontWeight: 700 }}>ajouter une {fieldName}</div>
                    </div>
                </Upload.Dragger>
            ) : (
                <div style={{ width: "100%", textAlign: "center" }}>
                    <div className="ant-upload ant-upload-drag" style={{ padding: 12 }}>
                        <div>Fichier : {fileListField[0].name}</div>
                        <div>
                            {fieldName === 'image' ? <img
                                style={{ width: "100%", maxWidth: 200 }}
                                src={fileToUrl}
                            /> :
                                <audio controls src={fileToUrl} style={{ width: "100%" }}>
                                    Your browser does not support the
                                    <code>audio</code> element.
                                </audio>}
                        </div>
                        <Upload
                            maxCount={1}
                            beforeUpload={() => false}
                            onChange={(e) => onChange(e.fileList, { fieldName })}
                            fileList={fileListField}
                            showUploadList={false}
                        >
                            <Button icon={<SwapOutlined />} size="small">
                                Changer
                            </Button>
                        </Upload>

                        <Button
                            icon={<DeleteOutlined />}
                            size="small"
                            style={{ marginLeft: 4 }}
                            onClick={() => deleteFile({ fieldName })}
                        >
                            Supprimer
                        </Button>
                    </div>
                </div>
            )}
        </Form.Item>
    )
}
export default UploadCustom