/* eslint-disable react/prop-types */
import React, { useEffect, useState } from "react";
import { doc, setDoc } from "firebase/firestore";
import { firestore } from "../firebase-config";
import { Button, Modal, Form, Input, Space, message } from "antd";
import { MinusCircleOutlined, PlusOutlined } from "@ant-design/icons";
import { BarsOutlined } from "@ant-design/icons";
import { findTreeLinksChannel } from "../functions";

function PopLinks(props) {
    const [linksOpen, setLinksOpen] = useState(false);
    const [loading, setLoading] = useState(false);
    const [linksList, setLinksList] = useState();
    const { name: channelId, label } = props.currentChannel
    useEffect(() => {
        const query = async () => {
            setLoading(true);

            const mapSnap = await findTreeLinksChannel(
                channelId,
                "treelinks"
            );
            setLinksList(mapSnap);
            setLoading(false);
        };
        linksOpen && query();
    }, [linksOpen, channelId]);

    const success = (msg) => {
        message.success(msg);
    };

    const error = (e) => {
        console.log(e)
        message.error("This is an error message");
    };

    const onFinish = async (values) => {
        const chaineRef = doc(firestore, "channels", channelId);
        try {
            await setDoc(chaineRef, values, { merge: true });
            success("enregistrement réussi");
        } catch (e) {
            error(e);
        }
    };

    return (
        <>
            <Button
                style={props.style || null}
                onClick={() => setLinksOpen(true)}
                icon={<BarsOutlined />}
            >
                Raccourcis
            </Button>
            <Modal
                // eslint-disable-next-line react/prop-types
                title={label}
                visible={linksOpen}
                onCancel={() => setLinksOpen(false)}
                footer={[]}
            >
                <div style={{ textAlign: "center" }}>
                    {!loading && (
                        <Form
                            name="dynamic_form_nest_item"
                            onFinish={onFinish}
                            autoComplete="off"
                            initialValues={{
                                treelinks: linksList,
                            }}
                        >
                            <Form.List name="treelinks">
                                {(fields, { add, remove }) => (
                                    <>
                                        {fields.map(({ key, name, ...restField }) => (
                                            <Space
                                                key={key}
                                                style={{
                                                    display: "flex",
                                                    marginBottom: 8,
                                                }}
                                                align="baseline"
                                            >
                                                <Form.Item
                                                    {...restField}
                                                    name={[name, "name"]}
                                                    rules={[
                                                        {
                                                            required: true,
                                                            message: "Missing first name",
                                                        },
                                                    ]}
                                                >
                                                    <Input placeholder="Name of your link" />
                                                </Form.Item>
                                                <Form.Item
                                                    {...restField}
                                                    name={[name, "link"]}
                                                    rules={[
                                                        {
                                                            required: true,
                                                            message: "Missing last name",
                                                        },
                                                    ]}
                                                >
                                                    <Input placeholder="Url of your link" />
                                                </Form.Item>

                                                <MinusCircleOutlined onClick={() => remove(name)} />
                                            </Space>
                                        ))}
                                        <Form.Item>
                                            <Button
                                                type="dashed"
                                                onClick={() => add()}
                                                block
                                                icon={<PlusOutlined />}
                                            >
                                                Add field
                                            </Button>
                                        </Form.Item>
                                    </>
                                )}
                            </Form.List>
                            <Form.Item>
                                <Button type="primary" htmlType="submit">
                                    Submit
                                </Button>
                            </Form.Item>
                        </Form>
                    )}
                </div>
            </Modal>
        </>
    );
}

export default PopLinks;