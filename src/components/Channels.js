import React, { useState, useEffect } from "react";
import { useAuthValue } from "./../providers/AuthContext";
import EditItem from "./EditItem"
import { getChannelsList, getItemsList, getDefaultEp } from "../functions"
import { Select, Col, Row, Table, Modal, Tooltip, Typography, Grid, Button } from "antd";
import { ExclamationCircleOutlined, DeleteOutlined, QrcodeOutlined, SearchOutlined, } from "@ant-design/icons";
import PopLinks from '../components/PopLinks';
import QrLinks from '../components/QrLinks';

import { doc, updateDoc } from "firebase/firestore";
import { firestore } from "../firebase-config";
import { Link } from "react-router-dom";
import EditChannel from "./EditChannel";
const { Option } = Select;
const { Title } = Typography;

const { useBreakpoint } = Grid;

const podcastIs = <div style={{ width: 300, textAlign: 'center', margin: '24px auto' }} >Un podcast est une chaine (ou une emmission). Il regroupe des épisodes, qui se suivent (serie), ou independants.</div>
const episodeIs = <div style={{ width: 300, textAlign: 'center', margin: '24px auto' }} >Les épisodes peuplent le podcast, à chaque épisode correspond un fichier audio </div>


const columns = (defaultEP, currentChannel, currentUser, setRefresh) => [
    {
        title: "Titre",
        dataIndex: "title",
        key: "title",
        width: '50%',
        render: (_, record) => {
            return <> {record.title} <Tooltip title="certains sont champs manquants"><ExclamationCircleOutlined style={{ color: 'red' }} /></Tooltip></>
        }
    },


    {
        title: "",
        key: "action",

        render: (_, record) => {
            return (
                <div style={{ textAlign: 'right' }}>

                    {record.uuid === defaultEP && <Link to={'/qr/' + currentChannel.label} target='_blank'> <QrcodeOutlined style={{ color: '#1890ff' }} /></Link>}
                    <SearchOutlined style={{ marginLeft: '6%', color: '#1890ff', }} />
                    <EditItem id={record.uuid} style={{ color: '#1890ff', marginLeft: '6%' }} currentItem={record} currentChannel={currentChannel.key} currentUser={currentUser} setRefresh={setRefresh} icon />
                    <DeleteOutlined style={{ color: "red", marginLeft: '6%' }} />

                </div>
            )
        },
    },
];


const Channels = () => {
    const { currentUser } = useAuthValue()
    const [data, setData] = useState();
    const [currentChannel, setCurrentChannel] = useState();
    const [defaultEp, setDefaultEp] = useState()
    const [loading, setLoading] = useState(true);
    const [channelsList, setChannelsList] = useState();
    const [refresh, setRefresh] = useState(false);
    const [refreshListChannel, setRefreshListChannel] = useState(true);
    const screensSize = useBreakpoint();
    const isXs = !screensSize.sm

    useEffect(() => {
        const fetchListChannels = async () => {
            const res = await getChannelsList(currentUser.uid, true);

            const firstChannel = res ? res[0] : null;
            setCurrentChannel(firstChannel);
            setChannelsList(res);

            setRefreshListChannel(false)
            setRefresh(true)
        };

        refreshListChannel && fetchListChannels();
    }, [refreshListChannel])

    useEffect(() => {
        const fetchListItems = async () => {
            const res = await getItemsList(currentChannel.value);
            res.forEach(item => item.key = item.uuid)

            const currentDefaultEp = await getDefaultEp(currentChannel.value)
            setDefaultEp(currentDefaultEp)
            setData(res);
            setLoading(false);
            setRefresh(false);
        };
        refresh && currentChannel && fetchListItems();
        refresh && !currentChannel && setLoading(false);

    }, [refresh]);

    // const chaineLabel = currentChannel?.key
    //     ? channelsList.find((c) => c.name === currentChannel.label).key
    //     : null;
    const changeDefaultEp = async (selectedRowKeys) => {
        const channelDoc = doc(firestore, "channels", currentChannel.key);
        await updateDoc(channelDoc, { defaultEp: selectedRowKeys[0] });

        setRefresh(true)
    }

    const confirm = (selectedRowKeys) => {
        Modal.confirm({
            title: 'Confirm',
            icon: <ExclamationCircleOutlined />,
            content: 'Etes-vous certain de vouloir changer la cible du QRcode ?',
            okText: 'OUI',
            cancelText: 'NON',
            onOk: () => {
                changeDefaultEp(selectedRowKeys)
            },
            onCancel: () => {
                setLoading(true)
                setRefresh(true)
            }
        });
    };

    const handleChangeCurrentChannel = (e) => {
        setLoading(true)
        setCurrentChannel(e)
        setRefresh(true)
    }

    const buttonStyleBlock = isXs ? { width: '100%', marginBottom: 8, marginRight: 8 } : { marginRight: 8 };

    let showListChannel = !loading && currentChannel ? true : false
    let showEditChannel = !loading && currentChannel ? true : false
    let showPoplink = !loading && data?.length ? true : false
    let showQrLinks = !loading && data?.length ? true : false
    let showEditItem = !loading && data?.length ? true : false


    return <>
        <Title type="success" level={5} style={{ padding: '12px 24px 0px 24px' }} >PODCAST</Title>
        <>
            <Row style={{ padding: '12px 24px 4px 24px', fontWeight: 700, width: "100%", borderBottom: "1px solid #ddd" }} align="middle">

                <Col xs={24} sm={24} md={12} style={{ marginBottom: 12 }}>
                    Mes podcast :{" "}
                    {showListChannel ? <Select
                        value={currentChannel}
                        style={{ width: 300 }}
                        size="large"
                        onChange={(e) => handleChangeCurrentChannel(e)}
                        labelInValue
                    >
                        {channelsList.map((c) => (
                            <Option value={c.name} key={c.name}>
                                {c.label}
                            </Option>
                        ))}

                    </Select> : "aucun podcast actuelement"}
                </Col>

                <Col xs={24} sm={24} md={12} style={{ textAlign: "right" }}>
                    <EditChannel block={isXs} style={{ marginRight: 8, marginBottom: 12 }} currentUser={currentUser} setRefreshListChannel={setRefreshListChannel} />
                    <Button block={isXs} style={{ marginRight: 8, marginBottom: 12 }} type='primary' danger >Importer un RSS</Button>
                </Col>

            </Row>

            <Row style={{ padding: 12, fontWeight: 700, width: "100%" }} align="middle">
                <Col span={24}>
                    {showEditChannel && <EditChannel block={isXs} style={{ marginRight: 8, marginBottom: 12 }} channelId={currentChannel.key} currentUser={currentUser} setRefreshListChannel={setRefreshListChannel} />}
                    {showPoplink && <PopLinks style={buttonStyleBlock} currentChannel={currentChannel || null} />}
                    {showQrLinks && <QrLinks style={buttonStyleBlock} currentChannel={currentChannel || null} />}
                    {showEditItem && <EditItem style={buttonStyleBlock} id='null' currentChannel={currentChannel.key} currentUser={currentUser} setRefresh={setRefresh} />}
                </Col>
            </Row>

        </>


        {!loading && !currentChannel &&
            <div style={{ textAlign: "center", width: '100%' }}>
                <Title type="success" level={3} style={{ margin: 24, color: "#999" }} >{"Vous n'avez pas encore de podcast"}</Title>
                <Title type="success" level={5} style={{ margin: 24, color: "#666" }} >Commencer par ajouter un podcast, vous pourrez ensuite lui ajouter des épisodes</Title>
                <EditChannel style={{ marginRight: 8, marginBottom: 12 }} id='null' currentUser={currentUser} setRefreshListChannel={setRefreshListChannel} />
                <Button style={{ marginRight: 8, marginBottom: 12 }} type='primary' danger >Importer un RSS</Button>
                {podcastIs}
            </div>
        }

        {!loading && !data?.length && currentChannel &&
            <div style={{ textAlign: "center", width: '100%' }}>
                <Title type="success" level={3} style={{ margin: 24, color: "#999" }} >{`Le podcast "${currentChannel.label}" n'a pas encore d'épisode`}</Title>
                <Title type="success" level={5} style={{ margin: 24, color: "#666" }} >Commencer par ajouter un épisode, vous pourrez ensuite publier ce podcast</Title>
                <EditItem style={buttonStyleBlock} id='null' currentChannel={currentChannel.key} currentUser={currentUser} setRefresh={setRefresh} />
                {episodeIs}
            </div>
        }

        {!loading && data?.length > 0 && (
            <Table
                columns={columns(defaultEp, currentChannel, currentUser, setRefresh)}
                dataSource={data}
                style={{ width: 'calc(100% -24px)', marginLeft: 12, marginRight: 12 }}
                rowSelection={{
                    onChange: async (selectedRowKeys) => {
                        confirm(selectedRowKeys);
                    },

                    type: 'radio',
                    defaultSelectedRowKeys: [defaultEp],
                }}
            />
        )}
    </>
}
export default Channels

