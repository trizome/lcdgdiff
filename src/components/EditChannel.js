/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */
import React, { useEffect, useState } from "react";
import { Button, Form, Input, Upload, Select, Modal, Row, Col, Grid } from 'antd';
import { PictureOutlined, PlusOutlined, EditOutlined } from "@ant-design/icons";
import { saveChannel } from "../functions/saveData";
import { findChannelByUUID } from "../functions/";
import UploadCustom from "./UploadCustom";


import moment from 'moment'
import {
    doc, updateDoc
} from "firebase/firestore";
import { firestore } from "../firebase-config";
const { useBreakpoint } = Grid;

const { Option } = Select;
const normFile = (e) => {
    if (Array.isArray(e)) {
        return e;
    }
    return e?.fileList;
};

const EditChannel = (props) => {
    const { currentUser, channelId } = props
    const [isModalVisible, setIsModalVisible] = useState(false);
    const [loading, setLoading] = useState(true);
    const [data, setdata] = useState();
    const [inUpload, setInUpload] = useState(false);
    const [form] = Form.useForm();
    const screensSize = useBreakpoint();
    const isXs = !screensSize.sm


    useEffect(() => {
        const fetchChannel = async () => {
            const resChannel = await findChannelByUUID(channelId)
            resChannel.pubDate = moment(resChannel.pubDate);
            const { title: titleImage, url: urlImage = null } = resChannel.image
            resChannel.defaultFileListImage = [{
                uuid: resChannel.uuid, name: titleImage, url: urlImage, source: "bdd",
            }]
            setdata(resChannel)
            setLoading(false)
        }
        channelId ? fetchChannel() : setLoading(false)

    }, [channelId])

    const onFinish = async (values) => {
        await saveChannel(values, currentUser.uid, setInUpload, channelId, data?.defaultFileListImage)
        setIsModalVisible(false)
        props.setRefreshListChannel(true)
    };


    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };
    if (loading) return 'loading'

    return (<>
        {channelId
            ? <Button icon={<EditOutlined />} block={props.block} style={props.style} type='ghost' onClick={() => setIsModalVisible(true)}>Modifier ce podcast</Button>
            : <Button icon={<PlusOutlined />} block={props.block} style={props.style} type='primary' onClick={() => setIsModalVisible(true)}>ADD PODCAST</Button>
        }
        <Modal
            footer={null}
            title={props.title || 'rien'} visible={isModalVisible} onOk={() => setIsModalVisible(false)} onCancel={() => setIsModalVisible(false)}>

            {inUpload?.currentUpload ? <div><span>{inUpload.currentUpload}</span>{' : '}<span>{inUpload.progress}</span></div> :
                < Form
                    layout={"vertical"}
                    onFinish={onFinish}
                    onFinishFailed={onFinishFailed}
                    autoComplete="off"
                    initialValues={data || {}}
                    form={form}
                >

                    {/*  TITLE */}
                    <Form.Item
                        label="Titre du podcast"
                        name="title"
                        rules={[
                            {
                                required: true,
                                message: 'Please input a title!',
                            },
                        ]}
                    >
                        <Input />
                    </Form.Item>


                    {/*  IMAGE TEST*/}
                    <UploadCustom setFieldsValue={form.setFieldsValue} fieldName='image' defaultFileList={data?.defaultFileListImage} />


                    {/* CATEGORIES */}
                    <Form.Item
                        label="Catégories"
                        name="category"
                        rules={[
                            {
                                required: true,
                                message: 'enter une ou plusieurs categories',
                            },
                        ]}
                    >
                        <Select
                            mode="tags"
                            placeholder="Tags Mode"
                        >
                            {[<Option key='famille'>Famille</Option>, <Option key='aventure'>Aventure</Option>]}
                        </Select>
                    </Form.Item>

                    {/*  DESCRIPTION */}
                    <Form.Item
                        label="Description"
                        name="description"
                        rules={[
                            {
                                required: true,
                                message: 'enter a description!',
                            },
                        ]}
                        extra="Saisissez une description du podcast"
                    >
                        <Input.TextArea showCount rows={4} />
                    </Form.Item>

                    <Row gutter={8}>
                        <Col span={isXs ? 24 : 12}>
                            {/*  EPISODETYPE */}
                            <Form.Item
                                label="Type de podcast"
                                name="episodeType"
                                rules={[
                                    {
                                        required: true,
                                        message: 'enter a type !',
                                    },
                                ]}
                            >
                                <Select >
                                    <Option value="episodic">Episode</Option>
                                    <Option value="serial">Serie</Option>
                                </Select>

                            </Form.Item>
                        </Col>
                        <Col span={isXs ? 24 : 12}>
                            {/*  EXPLICIT */}
                            <Form.Item
                                label="Language Explicite"
                                name="explicit"
                                rules={[
                                    {
                                        required: true,
                                        message: 'enter a explicit!',
                                    },

                                ]}
                            >
                                <Select>
                                    <Option value="true">Oui</Option>
                                    <Option value="false">NON</Option>
                                </Select>

                            </Form.Item>
                        </Col>

                    </Row>

                    <Row gutter={8}>
                        <Col span={isXs ? 24 : 12}>
                            {/*  LANGUE */}
                            <Form.Item
                                label="Langue"
                                name="language"
                                rules={[
                                    {
                                        required: true,
                                        message: 'enter a explicit!',
                                    },

                                ]}
                            >
                                <Select >
                                    <Option value="fr">Francais</Option>
                                    <Option value="en">Anglais</Option>
                                    <Option value="es">Espagnol</Option>
                                    <Option value="it">Italien</Option>
                                </Select>

                            </Form.Item>
                        </Col>
                        <Col span={isXs ? 24 : 12}>
                            {/*  OWNER */}
                            <Form.Item
                                label="Auteur"
                                name="owner"
                                rules={[
                                    {
                                        required: true,
                                        message: 'enter a date!',
                                    },
                                ]}
                            >
                                <Input />
                            </Form.Item>
                        </Col>

                    </Row>
                    {/*  mail */}
                    <Form.Item
                        label="email"
                        name="email"
                        rules={[
                            {
                                required: true,
                                message: 'enter a date!',
                            },
                        ]}
                    >
                        <Input />
                    </Form.Item>
                    {/*  QRCODE */}
                    {/* <Form.Item
                        name="defaultEp"
                        valuePropName="checked"
                        wrapperCol={{
                            offset: 2,
                            span: 22,
                        }}
                    >
                        <Checkbox>Cet épisode devient la cible du qr code</Checkbox>
                    </Form.Item> */}

                    <Form.Item
                        wrapperCol={{
                            offset: 0,
                            span: 24,
                        }}
                        style={{ textAlign: "center" }}
                    >
                        <Button  >
                            Annuler
                        </Button>
                        <Button type="primary" htmlType="submit">
                            Valider
                        </Button>
                    </Form.Item>
                </Form >}
        </Modal ></>
    );
};

export default EditChannel