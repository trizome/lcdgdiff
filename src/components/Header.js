import React from 'react';
import { PageHeader, Button, Tooltip, Typography, Popover, Card, Avatar } from 'antd';
import { useNavigate, useLocation } from 'react-router-dom';

import LoginRegister from '../components/LoginRegister';
import LoginModal from '../components/LoginModal';
import { UserOutlined, SettingOutlined, EditOutlined, LoginOutlined } from '@ant-design/icons';
import { useAuthValue } from "./../providers/AuthContext";
import { signOut } from 'firebase/auth'
import { auth } from './../firebase-config'


const { Meta } = Card;

const setExtra = (currentUser) => {
    const content = currentUser?.emailVerified ? <Card
        style={{ width: 300 }}
        cover={
            <img
                alt="example"
                src="https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png"
            />
        }
        actions={[
            <SettingOutlined key="setting" />,
            <EditOutlined key="edit" />,
            <LoginOutlined key={'logout'} danger onClick={() => signOut(auth)} style={{ color: 'red' }} />
            // <Button key={'logout'} danger onClick={() => signOut(auth)} icon={<LoginOutlined />} />,
        ]}
    >
        <Meta
            avatar={<Avatar src="https://joeschmoe.io/api/v1/random" />}
            title={currentUser.displayName}
            description="This is the description"
        />
    </Card> : ""

    const location = useLocation();
    const navigate = useNavigate()
    const extra = [];
    currentUser?.emailVerified ?
        extra.push(
            <Tooltip placement="left" title="Mon compte" key={'compte'}>
                <Popover content={content} trigger="focus">
                    <Button icon={<UserOutlined />} />
                </Popover>
            </Tooltip>,
        )
        :
        extra.push(
            <LoginRegister key={'register'} />,
            <LoginModal key={'login'} />)


    // currentUser?.emailVerified && location.pathname === '/admin' && extra.unshift(<Button key={'add'} type='primary' icon={<PlusOutlined />}>Ajouter un podcast</Button>)
    currentUser?.emailVerified && location.pathname === '/' && extra.unshift(<Button type="link" key={'console'} onClick={() => navigate('admin')}>Mes podcast</Button>)
    return extra
}




const { Text } = Typography;
const Header = () => {
    const { currentUser } = useAuthValue()
    const navigate = useNavigate()
    const extra = setExtra(currentUser)
    return <PageHeader
        ghost={false}
        className="site-page-header"
        onBack={() => navigate('/')}
        title="PODI"
        subTitle={<Text type="secondary" strong >la plateforme de diffusion par LCDG prod.</Text>}
        extra={extra} />
}

export default Header