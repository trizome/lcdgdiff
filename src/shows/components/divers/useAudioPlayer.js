import { useState, useEffect } from "react";

function useAudioPlayer() {
  const [duration, setDuration] = useState();
  const [curTime, setCurTime] = useState();
  const [playing, setPlaying] = useState(false);
  const [clickedTime, setClickedTime] = useState();
  //const [dataArray, setDataArray] = useState([])

  useEffect(() => {
    const audio = document.getElementById("audio");
    // const context = new AudioContext()
    // //let src = context.createMediaElementSource(audio)
    // const analyser = context.createAnalyser()
    // //src.connect(analyser)
    // analyser.connect(context.destination)
    // analyser.fftSize = 512;
    // const bufferLength = analyser.frequencyBinCount
    // const uiny = new Uint8Array(bufferLength)
    // state setters wrappers
    const setAudioData = () => {
      setDuration(audio.duration);
      setCurTime(audio.currentTime);
      // setDataArray(analyser.getByteFrequencyData(uiny))
    };

    const setAudioTime = () => {
      setCurTime(audio.currentTime);
      //setDataArray(analyser.getByteFrequencyData(uiny))
    };

    // DOM listeners: update React state on DOM events
    audio.addEventListener("loadeddata", setAudioData);

    audio.addEventListener("timeupdate", setAudioTime);

    // React state listeners: update DOM on React state changes
    playing ? audio.play() : audio.pause();

    if (clickedTime && clickedTime !== curTime) {
      audio.currentTime = clickedTime;
      setClickedTime(null);
    }

    // effect cleanup
    return () => {
      audio.removeEventListener("loadeddata", setAudioData);
      audio.removeEventListener("timeupdate", setAudioTime);
    };
  });
  return {
    // dataArray,
    curTime,
    duration,
    playing,
    setPlaying,
    setClickedTime,
  };
}

export default useAudioPlayer;
