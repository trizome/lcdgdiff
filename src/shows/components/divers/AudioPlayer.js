/* eslint-disable react/prop-types */
import React, { useEffect } from "react";
import { Row, Slider, Col } from "antd";
import {
  PlayCircleTwoTone,
  PauseCircleTwoTone,
  LeftCircleTwoTone,
  RightCircleTwoTone,
} from "@ant-design/icons";

import useAudioPlayer from "./useAudioPlayer";

function msToTime(millis) {
  if (!millis) return "";
  var minutes = Math.floor(millis / 60);
  var seconds = (millis % 60).toFixed(0);
  return `${minutes}:${seconds < 10 ? "0" : ""}${seconds}`;
}

function AudioPlayer(props) {
  let { curTime, duration, playing, setPlaying, setClickedTime } =
    useAudioPlayer();
  const { url } = props.currentItem;
  useEffect(() => {
    if (url) {
      setPlaying(true);
    }
  }, [url, setPlaying]);

  return (
    <div
      className="audioPlayer"
      style={{ backgroundColor: "#000000CC", height: 110 }}
    >
      <audio id="audio">
        <source src={url} />
        Your browser does not support the <code>audio</code> element.
      </audio>

      <div
        style={{
          color: "#fff",
          lineHeight: "24px",
          background: "#FFFFFF11",
          borderRadius: "8px 8px 0px 0px ",
          padding: 4,
          marginBottom: 8,
        }}
      >
        titre
      </div>

      <div>
        <Row justify="center" align="middle" gutter={16}>
          <Col>
            <LeftCircleTwoTone
              style={{ fontSize: 24 }}
              onClick={() => setPlaying(false)}
            />
          </Col>
          <Col>
            {playing ? (
              <PauseCircleTwoTone
                style={{ fontSize: 36 }}
                onClick={() => setPlaying(false)}
              />
            ) : (
              <PlayCircleTwoTone
                style={{ fontSize: 36 }}
                onClick={() => setPlaying(true)}
              />
            )}
          </Col>
          <Col>
            <RightCircleTwoTone
              style={{ fontSize: 24 }}
              onClick={() => setPlaying(false)}
            />{" "}
          </Col>
        </Row>

        <Row justify="center" align="middle" gutter={16}>
          <Col style={{ color: "#FFF", textAlign: "right" }}>
            {msToTime(curTime)}
          </Col>
          <Col style={{ textAlign: "center" }}>
            <Slider
              value={curTime}
              onChange={(e) => setClickedTime(e)}
              onD
              min={0}
              max={duration}
              tooltipVisible={false}
              style={{ width: 100 }}
            />
          </Col>
          <Col style={{ color: "#FFF", textAlign: "left" }}>
            {msToTime(duration)}
          </Col>
        </Row>

      </div>
    </div>
  );
}

export default AudioPlayer;
