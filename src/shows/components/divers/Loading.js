const Loading = (props) => {
  const { message = "chargement" } = props;
  return (
    <div
      className="card"
      style={{
        marginTop: 100,
        textAlign: "center",
        padding: 24,
        fontSize: "1.5em",
        color: "#999",
      }}
    >
      {message}
    </div>
  );
};

export default Loading;
