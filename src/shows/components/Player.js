/* eslint-disable react/prop-types */
import React, { useEffect, useState } from "react";
//import "../App.css";
function Player(props) {
  const [currentTime, setCurrentTime] = useState(0);
  useEffect(() => {
  }, [currentTime]);

  const timeUpdate = (e) => {
    const realCurrent = e.target.currentTime;
    const roundCurrent = Math.floor(realCurrent / 10) * 10;
    e.target.currentTime !== Math.floor(e.target.currentTime / 10) * 10 &&
      setCurrentTime(roundCurrent);
  };
  return (
    <div className="player">
      <audio
        onPause={() => console.log("onPause")}
        onPlay={() => console.log("onPlay")}
        onEnded={() => console.log("onEnded")}
        onTimeUpdate={(e) => timeUpdate(e)}
        controls
        src={props.url}
        style={{
          borderRadius: 30,
          boxShadow: "0 0 18px 0 #66eeff",
          border: "2px solid #00000088",
        }}
      />
    </div>
  );
}

export default Player;
