

/* eslint-disable react/prop-types */
import React, { useState, useEffect } from "react";
import {
    MenuOutlined,
    CloseOutlined,
} from "@ant-design/icons";
import { Col, Drawer, Row } from "antd";
import { Link } from "react-router-dom";

import "./styles.css";
import { findChannelByName } from "../functions/index"



const drawer = {
    style: {
        position: "absolute",
    },
    drawerStyle: {
        position: "relative",
        backgroundColor: "#38a3e0",
        textAlign: "left",
        color: "#eee",
        fontWeight: 700,
    },
};

const LineItemTree = (props) => {
    const {
        uuid,
        title,
        url,
    } = props;


    return (
        <div
            onClick={() =>
                window.open(url, "_blank")
            }
            key={uuid}
            className="lineitem"
            style={{ border: `1px solid #ffffffdd`, }}
        >

            <div>{title}</div>  <div style={{
                width: '80%', fontWeight: 400, color: "#ffffff44", fontSize: '0.8em', whiteSpace: 'nowrap',
                overflow: 'hidden', textOverflow: 'ellipsis'
            }}>{url}</div>



        </div>
    );
};

function TreeLinks() {
    const [visible, setVisible] = useState(false);
    const [currentChannel, setCurrentChannel] = useState(false);
    const [loading, setLoading] = useState(true);
    const [urlChannel/*, setUrlChannel*/] = useState(decodeURI(location.pathname.split('/')[2]));

    useEffect(() => {
        const fetchFindChannel = async () => {
            const res = await findChannelByName(urlChannel)
            setCurrentChannel(res)
            setLoading(false)
        };
        fetchFindChannel();
    }, [])


    if (loading) return 'loading'
    return (
        <div className="backLayout">
            <div className="showLayout">
                <div className="wraperPlayerHeader">
                    <div className="playerHeader playerHeaderPage">
                        <MenuOutlined onClick={() => setVisible(true)} /> by LCDG prod.
                    </div>
                </div>
                <Drawer
                    mask={true}
                    closable={false}
                    onClose={() => setVisible(false)}
                    visible={visible}
                    placement="left"
                    width={200}
                    getContainer={false}
                    style={drawer.style}
                    drawerStyle={drawer.drawerStyle}
                >
                    <div className="playerHeader playerHeaderDrawer">
                        by LCDG Prod.
                        <CloseOutlined
                            onClick={() => setVisible(false)}
                            style={{ marginLeft: 70 }}
                        />
                    </div>

                    <div style={{ fontWeight: 400, marginTop: 36 }}>
                        LCDG prod. propose une plateforme de diffusion de podcasts. Elle a
                        pour ambition de proposer des services de qualité pour un prix le
                        plus juste possible.
                    </div>
                    <div className="drawerLine">
                        <Link to="/">Tous nos podcasts </Link>
                    </div>
                    <div className="drawerLine">
                        <Link to="/">Je suis podcasteur</Link>
                    </div>
                    <div style={{ marginTop: 36, textAlign: "center" }}>
                        <div
                            className="momentHead"
                            style={{ color: "#ffffff99", fontWeight: 700 }}
                        >
                            A DECOUVRIR
                        </div>
                        <img
                            style={{ marginTop: 8 }}
                            src={
                                "https://i.scdn.co/image/6c97817aace4a70f1f9158671adf8772d5e3f018"
                            }
                            alt="proxémie - kevy shako - podcast"
                            width="150"
                            height="150"
                        />
                        <div>Proxémie </div>
                    </div>
                </Drawer>
                <Row className="currentChannel">
                    <Col span={8}>
                        <img
                            className="card"
                            style={{ width: 90, height: 90, marginTop: 0 }}
                            src={currentChannel.image.url}
                            alt={currentChannel.image.title}
                            width="90"
                            height="90"
                        />
                    </Col>
                    <Col span={16} className='info'>
                        <div className="playertitle"
                        >
                            {currentChannel.title}
                        </div>
                        <div className='copyright'>
                            {currentChannel.copyright}
                        </div>
                        <div className='playerdescription' >
                            {currentChannel.description}
                        </div>
                    </Col>
                </Row>
                <div className="listItemsContainer">
                    {currentChannel.treelinks.map((item) => (
                        <LineItemTree
                            key={item.name}
                            uuid={item.name}
                            title={item.name}
                            url={item.link}
                        />
                    ))}
                </div>


            </div>
        </div>
    );
}
export default TreeLinks;
