/* eslint-disable react/prop-types */
import React, { useState, useEffect } from "react";
import {
  MenuOutlined,
  CloseOutlined,
  PlayCircleTwoTone,
  PlayCircleFilled,
} from "@ant-design/icons";
import { Col, Drawer, Row } from "antd";
import { Link } from "react-router-dom";
import "./styles.css";
import AudioPlayer from "./components/divers/AudioPlayer";
import { playListData } from "../functions/index"



const drawer = {
  style: {
    position: "absolute",
  },
  drawerStyle: {
    position: "relative",
    backgroundColor: "#38a3e0",
    textAlign: "left",
    color: "#eee",
    fontWeight: 700,
  },
};

const LineItem = (props) => {
  const {
    currentItem,
    setCurrentItem,
    uuid,
    title = "test",
    description = "description",
    url
  } = props;
  const isCurrent = currentItem?.uuid === uuid;
  return (
    <Row
      onClick={() =>
        !isCurrent
          ? setCurrentItem({ title, url: url + "?" + Math.random(), uuid })
          : null
      }
      key={uuid}
      className="lineitem"
      style={{ border: `1px solid ${isCurrent ? "#ffffffcc" : "#ffffff66"}` }}
    >
      <Col span={20}>
        {title}
        <br />
        {description}
      </Col>
      <Col span={4}>
        {!isCurrent ? (
          <PlayCircleFilled style={{ fontSize: 40, color: "#ffffffaa" }} />
        ) : (
          <PlayCircleTwoTone style={{ fontSize: 40, color: "#38a3e0" }} />
        )}
      </Col>
    </Row>
  );
};

function Playlist() {
  const [visible, setVisible] = useState(false);
  const [currentItem, setCurrentItem] = useState(false);
  const [refreshAudio, setRefreshAudio] = useState(false);
  const [data, setData] = useState(false);
  const [loading, setLoading] = useState(true);
  const [urlChannel/*, setUrlChannel*/] = useState(decodeURI(location.pathname.split('/')[2]));


  useEffect(() => {
    const fetchQRCodeData = async () => {
      const res = await playListData(urlChannel)
      setData(res)
      setLoading(false)
    };
    fetchQRCodeData();
  }, [])

  useEffect(() => {
    setRefreshAudio(true);
  }, [currentItem]);
  useEffect(() => {
    setRefreshAudio(false);
  }, [refreshAudio]);

  if (loading) return 'loading'
  const { channel, items } = data
  return (
    <div className="backLayout">
      <div className="showLayout">
        <div className="wraperPlayerHeader">
          <div className="playerHeader playerHeaderPage">
            <MenuOutlined onClick={() => setVisible(true)} /> by LCDG prod.
          </div>
        </div>
        <Drawer
          mask={true}
          closable={false}
          onClose={() => setVisible(false)}
          visible={visible}
          placement="left"
          width={200}
          getContainer={false}
          style={drawer.style}
          drawerStyle={drawer.drawerStyle}
        >
          <div className="playerHeader playerHeaderDrawer">
            by LCDG Prod.
            <CloseOutlined
              onClick={() => setVisible(false)}
              style={{ marginLeft: 70 }}
            />
          </div>

          <div style={{ fontWeight: 400, marginTop: 36 }}>
            LCDG prod. propose une plateforme de diffusion de podcasts. Elle a
            pour ambition de proposer des services de qualité pour un prix le
            plus juste possible.
          </div>
          <div className="drawerLine">
            <Link to="/">Tous nos podcasts </Link>
          </div>
          <div className="drawerLine">
            <Link to="/">Je suis podcasteur</Link>
          </div>
          <div style={{ marginTop: 36, textAlign: "center" }}>
            <div
              className="momentHead"
              style={{ color: "#ffffff99", fontWeight: 700 }}
            >
              A DECOUVRIR
            </div>
            <img
              style={{ marginTop: 8 }}
              src={
                "https://i.scdn.co/image/6c97817aace4a70f1f9158671adf8772d5e3f018"
              }
              alt="proxémie - kevy shako - podcast"
              width="150"
              height="150"
            />
            <div>Proxémie </div>
          </div>
        </Drawer>
        <Row className="currentChannel">
          <Col span={8}>
            <img
              className="card"
              style={{ width: 90, height: 90, marginTop: 0 }}
              src={channel.image.url}
              alt={channel.image.title}
              width="90"
              height="90"
            />
          </Col>
          <Col span={16} className='info'>
            <div className="playertitle"
            >
              {channel.title}
            </div>
            <div className='copyright'>
              {channel.copyright}
            </div>
            <div className='playerdescription' >
              {channel.description}
            </div>
          </Col>
        </Row>
        <div className="listItemsContainer">
          {items.map((item) => (
            <LineItem
              key={item.uuid}
              uuid={item.uuid}
              title={item.title}
              description={item.description}
              setCurrentItem={setCurrentItem}
              currentItem={currentItem}
              url={item.audio.url}
            />
          ))}
        </div>

        {currentItem && !refreshAudio && (
          <AudioPlayer currentItem={currentItem} />
        )}
      </div>
    </div>
  );
}
export default Playlist;
