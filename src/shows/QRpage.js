/* eslint-disable react/prop-types */
import React, { useEffect } from 'react'
import Player from "./components/Player";
import Share from "./components/share/Share";
import { MenuOutlined, CloseOutlined } from "@ant-design/icons";
import { Button, Drawer } from "antd";
import { useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import { QRCodeData } from "../functions/index"
import "./styles.css";
const drawer = {
    style: {
        position: "absolute",
    },
    drawerStyle: {
        backgroundColor: "#38a3e0",
        // borderRight: "4px solid #ffffff44",
        textAlign: "left",
        color: "#eee",
        fontWeight: 700,
    },
};

function Home() {
    const [visible, setVisible] = useState(false);
    // eslint-disable-next-line no-unused-vars
    const [data, setData] = useState(false);
    const [loading, setLoading] = useState(true);
    const [urlChannel/*, setUrlChannel*/] = useState(decodeURI(location.pathname.split('/')[2]));

    const navigate = useNavigate();

    useEffect(() => {
        const fetchQRCodeData = async () => {
            const res = await QRCodeData(urlChannel)
            setData(res)
            setLoading(false)
        };
        fetchQRCodeData();
    }, [])

    if (loading) return "loading"
    const { channel: { copyright }, item: { image, title, audio } } = data
    return (
        <>
            <div className="showLayout">
                <div className="wraperPlayerHeader">
                    <div className="playerHeader playerHeaderPage">
                        <MenuOutlined onClick={() => setVisible(true)} /> by LCDG prod.
                    </div>
                </div>
                <Drawer
                    mask={true}
                    closable={false}
                    onClose={() => setVisible(false)}
                    visible={visible}
                    placement="left"
                    width={200}
                    getContainer={false}
                    style={drawer.style}
                    drawerStyle={drawer.drawerStyle}
                >
                    <div className="playerHeader playerHeaderDrawer">
                        by LCDG Prod.
                        <CloseOutlined
                            onClick={() => setVisible(false)}
                            style={{ marginLeft: 70 }}
                        />
                    </div>

                    <div style={{ fontWeight: 400, marginTop: 36 }}>
                        LCDG prod. propose une plateforme de diffusion de podcasts. Elle a
                        pour ambition de proposer des services de qualité pour un prix le
                        plus juste possible.
                    </div>
                    <div className="drawerLine">
                        <Link to="/">Tous nos podcasts </Link>
                    </div>
                    <div className="drawerLine">
                        <Link to="/">Je suis podcasteur</Link>
                    </div>
                    <div style={{ marginTop: 36, textAlign: "center" }}>
                        <div
                            className="momentHead"
                            style={{ color: "#ffffff99", fontWeight: 700 }}
                        >
                            A DECOUVRIR
                        </div>
                        <img
                            style={{ marginTop: 8 }}
                            src={
                                "https://i.scdn.co/image/6c97817aace4a70f1f9158671adf8772d5e3f018"
                            }
                            alt="proxémie - kevy shako - podcast"
                            width="150"
                            height="150"
                        />
                        <div>Proxémie </div>
                    </div>
                </Drawer>
                <img
                    className="card"
                    src={image['url']}
                    alt={image['title']}
                    width="264"
                    height="264"
                />
                <div className="title">{title}</div>
                <div className="artist">{copyright}</div>

                <Player url={audio.url} />
                <div className="buttonlinks">
                    <Button
                        shape="round"
                        type="primary"
                        size="small"
                        onClick={() => navigate(`/channel/${urlChannel}`)}
                    >
                        Tous les épisodes
                    </Button>
                    <Button
                        style={{ color: "#ffffffcc" }}
                        shape="round"
                        type="link"
                        size="small"
                        onClick={() => navigate(`/tree/${urlChannel}`)}
                    >
                        Autres accès
                    </Button>
                </div>

                <div className="share">
                    <Share
                        size={24}
                        shareUrl={`https://qrCode.lcdgprod.com/${"lcdg"}`}
                        title={`LCDG - ${"lcdg"}`}
                    />
                </div>
            </div>
        </>
    );
}
export default Home;