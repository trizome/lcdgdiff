
import 'antd/dist/antd.min.css'
import React, { useState, useEffect } from "react";
import { Routes, Route, useLocation } from "react-router-dom";

import { AuthProvider } from "./providers/AuthContext";
import { auth } from './firebase-config'
import { onAuthStateChanged } from 'firebase/auth'

import Home from "./screens/Home";
import Admin from "./screens/Admin";
import QRpage from "./shows/QRpage";
import Playlist from "./shows/Playlist";
import TreeLinks from "./shows/TreeLinks";


import VerifyEmail from "./components/VerifyEmail";
import Header from "./components/Header";

function App() {
  const [currentUser, setCurrentUser] = useState(null)
  const [timeActive, setTimeActive] = useState(false)
  const location = useLocation();
  useEffect(() => {
    onAuthStateChanged(auth, (user) => {
      setCurrentUser(user)
    })
  }, [])

  return (

    <AuthProvider value={{ currentUser, timeActive, setTimeActive }}>
      <div className="site-page-header-ghost-wrapper">
        {(location.pathname === '/admin' || location.pathname === '/') && <Header />}
      </div>
      <Routes>

        <Route path="/" element={<Home />} />
        <Route path="admin" element={<Admin />} />
        <Route path="qr/:id" element={<QRpage />} />
        <Route path="channel/:id" element={<Playlist />} />
        <Route path="tree/:id" element={<TreeLinks />} />
        <Route path="verify-email" element={<VerifyEmail />} />

      </Routes>

    </AuthProvider>

  );
}

export default App;